export default function QuoteHome() {
  return (
    <div className="container my-5 mx-auto d-flex justify-content-center flex-wrap">
      <div
        className="text-center px-5 py-1 mx-5"
        style={{
          minWidth: '300px',
          background: '#fdfbfb',
          boxShadow: 'rgba(0, 0, 0, 0.1) 0px 4px 12px'
        }}
      >
        <p>
          Let Love and Kindness <br />
          be the motivation behind all that you do
        </p>
        <p
          className="ml-2 pl-3 text-end"
          style={{fontSize: '0.8rem', lineHeight: '0.1'}}
        >
            1 Corinthians 16:14
 
        </p>
      </div>
    </div>

  )
}

