export default function GetToKnowHome() {
  return (
    <div className="container">
      <div
        className="
          parallax-2 parallax-height
          d-flex
          justify-content-center
          align-content-center
          py-1
          flex-wrap
          mx-5
        "
        id="about"
      >
        <div
          className="p-2 text-center"
          style={{
            width: '400px',
            backgroundColor: 'rgba(255, 255, 255, 0.9)',
            borderRadius: '50px 20px'
          }}
        >
          <h2>GET TO KNOW AZLA</h2>
          <p className="p-3">
            Hello, I am Azla from Sabah, Malaysia. currently, I am staying in
            Thailand as a full-time student. I'm majoring in Bioscience
            emphasize Community Public Health and Minoring in Education.
          </p>
        </div>
      </div>

      <div className="my-5 stack-img p-2 position-absolute">
        <img
          className="mx-1"
          src="./assets/home/vertical_stack.png"
          height="300px"
        />
      </div>
      <div className="h-gap my-5">
        <br />
      </div>
    </div>

  )
}
