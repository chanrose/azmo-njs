import Link from 'next/link'
import Image from 'next/image'


export default function HeaderLayout() {
  return (
      <div className="header">
      <nav className="navbar navbar-light fixed-top bg-light">
        <div className="container-fluid d-flex">
          <Link href="/">
          <a
            className="
              navbar-brand
              d-flex
              align-items-center
              mb-3 mb-md-0
              me-md-auto
              text-dark text-decoration-none
              logo
            "
          >
            <Image
              src="/assets/logo.png"
              className="bi ml-1 p-1"
              width="55"
              height="60"
            />
            <span className="fs-4 pt-2 ml-2">Azmo &emsp; </span>
          </a>
          </Link>

          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="offcanvas"
            data-bs-target="#offcanvasNavbar"
            aria-controls="offcanvasNavbar"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div
            className="offcanvas offcanvas-end"
            tabIndex="-1"
            id="offcanvasNavbar"
            aria-labelledby="offcanvasNavbarLabel"
          >
            <div className="offcanvas-header">
              <h5 className="offcanvas-title" id="offcanvasNavbarLabel">Azmo</h5>
              <button
                type="button"
                className="btn-close text-reset"
                data-bs-dismiss="offcanvas"
                aria-label="Close"
              ></button>
            </div>
            <div className="offcanvas-body">
              <ul className="navbar-nav justify-content-end flex-grow-1 pe-3">
                <li className="nav-item">
                  <Link href="/blogs">
                    <a className="nav-link">Blog</a> 
                  </Link>
                </li>

                <li className="nav-item">
                  <Link href="/collection">
                    <a className="nav-link">Collection</a>
                  </Link>
                </li>

                <li className="nav-item">
                  <Link href="/vlogs">
                    <a className="nav-link">Vlog</a>
                  </Link>
                </li>
                <li className="nav-item">
                  <Link href="/about">
                  <a className="nav-link">About</a>
                  </Link>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#contact">Contact</a>
                </li>
             </ul>
            </div>
          </div>
        </div>
      </nav>
    </div>
 
  )
}
